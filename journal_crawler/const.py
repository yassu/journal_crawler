from typing import Dict, Final

BROWSER_HEADER: Final[Dict[str, str]] = {
    'User-Agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) ' +
        'Chrome/88.0.4324.96 Safari/537.36'
}
